关于PSI
-------------
>PSI是一款基于SaaS模式(Software as a Service软件即服务)的企业管理软件。PSI以商贸企业的核心业务：采购、销售、库存（进销存）为切入点，最终目标是行业化的ERP解决方案。
>

PSI演示
-------------
>PSI的演示见：http://psi.butterfly.mopaasapp.com


源代码下载地址
-------------
>源码下载地址：http://git.oschina.net/crm8000/PSI/repository/archive/master

通过PSI源码在本地安装
-------------
> 本地环境搭建: http://my.oschina.net/u/134395/blog/376530
> 
> 用WampServer部署可能会遇到的问题：http://my.oschina.net/u/134395/blog/383754
> 
> lnmp部署参考：http://my.oschina.net/u/2525829/blog/532614
> 
> 用IIS如何配置：http://my.oschina.net/u/1415918/blog/511228

PSI的开源协议
-------------
>PSI的开源协议为GPL v3

>如果您有Sencha Ext JS的商业许可（参见： http://www.sencha.com/legal/#Sencha_Ext_JS ），那么PSI的开源协议为Apache License v2。
>在Apache License协议下，您可以闭源并私有化PSI的代码，作为您自己的商业产品来销售。

>
>关于开源协议和私有化PSI代码的详细解释，可以参考这里：http://my.oschina.net/u/134395/blog/524350

更多文档
-------------
> 更多文档，请参考：http://my.oschina.net/u/134395


PSI行业版
-------------
> 食品行业版   http://git.oschina.net/crm8000/PSI_Food_Industry
>
> 快递管理系统 
> web端：http://git.oschina.net/crm8000/PSI_FMS
> 微信端：http://git.oschina.net/crm8000/PSI_FMS_H5

如需要技术支持，请联系
-------------
>1、 普通QQ群： <a target="_blank" href="http://shang.qq.com/wpa/qunwpa?idkey=64808ce24f2a3186ccb1f37aad9ed591bcc4fb257d09749753aca98c6c73e400">414474186</a>
>
> 注意:
> PSI部署环境是：https://www.mopaas.com/
> 在其他部署环境下遇到的技术，需要自己解决，即使加入QQ群也不会有太多的帮助。
>
>2、 PSI 免费VIP QQ群：108111233
> 加入本群的要求：1、实名；2、认真地在应用PSI，并愿意分享实际应用案例。
>
>3、 付费技术支持QQ群：498771245

致谢
-------------
>PSI使用了如下开源软件，没有你们，就没有PSI
> 
>1、PHP (http://php.net/)
>
>2、MySQL (http://www.mysql.com/)
>
>3、ExtJS 4.2 (http://www.sencha.com/)
>
>4、ThinkPHP 3.2.3 (http://www.thinkphp.cn/)
>
>5、乱码 / pinyin_php (https://git.oschina.net/cik/pinyin_php)
>
>6、PHPExcel (https://github.com/PHPOffice/PHPExcel)
>
>7、TCPDF (http://www.tcpdf.org/)
>
>8、MUI (http://dev.dcloud.net.cn/mui/)